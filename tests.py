#! /usr/bin/env python3

"""Unit tests for shimmer."""


import shimmer

import unittest


class TestGenerateOutputFileName(unittest.TestCase):

    def test_generate_output_file_name(self):
        # List of tuples: (kwargs for the method, expected result)
        test_cases = [
            (
                {'file_name': 'test.asc'},
                'test.asc'
            ),
            (
                {'file_name': 'postfix.asc', 'postfix': '-applied'},
                'postfix-applied.asc'
            ),
            (
                {'file_name': 'out_dir.asc', 'output_dir': '../mydir'},
                '../mydir/out_dir.asc'
            ),
            (
                {'file_name': 'both.asc', 'postfix': '-postfix', 'output_dir': '~/also/the/dir/'},
                '~/also/the/dir/both-postfix.asc'
            ),
            (
                {'file_name': 'test_data/ZM-160-1.ASC', 'postfix': '_processed', 'output_dir': '~/tmp'},
                '~/tmp/ZM-160-1_processed.ASC'
            )
        ]

        for kwargs, exp_result in test_cases:
            result = shimmer.generate_output_file_name(**kwargs)
            self.assertEqual(result, exp_result)


class TestProcessDataLine(unittest.TestCase):

    def test_process_data_line(self):
        test_cases = [
            # Normal use case: decimal commas
            (
                '3995,000000	99,815155',
                ' 3995.000000  99.815155'
            ),
            # Failed parse
            (
                '4ts31.12   11.3',
                None
            ),
            # Normal use case: decimal dots
            (
                '3919.000000	99.809385',
                ' 3919.000000  99.809385'
            ),
            # Mixed decimal commas and dots
            (
                '1007.000000	94,440513',
                ' 1007.000000  94.440513'
            ),
        ]

        for line, exp_result in test_cases:
            result = shimmer.process_data_line(line)
            self.assertEqual(result, exp_result)


if __name__ == '__main__':
    unittest.main()
