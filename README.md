# shimmer

A Python script for cleaning up exported PerkinElmer FTIR spectra for easy
plotting. The scripts generates spectrum files with only data points
(wavenumber, transmitance) and ensures the numbers are in format with decimal
points instead of commas, so they can easily be plotted.


## Requirements

Shimmer requires [Python 3 interpreter](https://www.python.org/).


## Usage

Download `shimmer.py` Python script to the folder with data or to a folder
included in `PATH`. Use the following command to run it and display help:
```sh
python shimmer.py --help
```
or
```sh
./shimmer.py --help
```
or, if shimmer is in system `PATH`:
```sh
shimmer.py --help
```

Generally, two things need to be defined by command line arguments:
+ which input data to use
+ where to write output files

### Defining input data

Input data can be defined either as **passing a list of files** to the script
like so:
```sh
shimmer.py input_file1.asc file2.asc subdirectory/file3.asc
```
Or the script can process all files in the current working directory with a
**given extension**:
```sh
shimmer.py -e ".asc"
```
Extension matching is case-sensitive.

### Defining where to write output files

At least one of two options should be used to differentiate output files from
input files.

The first option is appending **postfix** to the input file name (before the
file extension). The following example would process `test.asc` to
`test_shimmer.asc`:
```sh
shimmer.py -p "_shimmer"
```
Postfixes starting with `-` should not be used as the confuse the
argument parser.

The second option is specifying a **directory** where all output files are
written. This example would write all output files in the directory
`processed`, that is located in the parent directory of the current working directory:
```sh
shimmer.py -o "../processed"
```

Output directory and postfix can be used in conjunction.

### Complete examples

To process a list of files and produce output files with `_clean` appended
to the source file names, use:
```sh
shimmer.py -p "_clean" file1.asc some_other_file.asc
```

To process every file with extension `.asc` in the current directory and write
all output files to subdirectory `processed`, use:
```sh
shimmer.py -o processed -e .asc
```
