#! /usr/bin/env python3

"""shimmer

A script for cleaning up exported PerkinElmer FTIR spectra for easy plotting.
"""


from argparse import ArgumentParser, Namespace
import logging
import pprint
import sys
import os


class Script:
    """Contains basic information about the script."""

    """Script name"""
    NAME = 'shimmer'

    """Script version"""
    VERSION = '1.1.0'

    """Date of the script version"""
    DATE = '2021-08-13'


def build_argument_parser() -> ArgumentParser:
    """Builds the `ArgumentParser` used to parse command line arguments for this script."""

    parser = ArgumentParser(description='Clean up exported PerkinElmer ASC FTIR '
        'spectra for easy plotting.')
    parser.add_argument('files', nargs='*', help='list of input files to process')
    parser.add_argument('-e', '--extension', action='store', metavar='EXTENSION',
        help='specify file extension to process all files of this type')
    parser.add_argument('-p', '--postfix', action='store', metavar='POSTFIX',
        help='postfix to append to the names of processed output files')
    parser.add_argument('-o', '--output-dir', action='store', metavar='OUTPUT_DIR',
        help='path to the directory to write processed output files into')
    parser.add_argument('-w', '--overwrite', action='store_true',
        help='overwrite existing output files')
    parser.add_argument('-v', '--verbose', action='store_true',
        help='enable verbose output')
    
    return parser

def parse_arguments() -> Namespace:
    """Parses command line arguments and returns them."""

    parser = build_argument_parser()
    args = parser.parse_args()
    return args


def main():
    """The main function of the script."""

    show_greeting()

    if len(sys.argv) == 1:
        parser = build_argument_parser()
        parser.print_usage()
        print('\nUse the "-h" or "--help" flag to display help.')
        return        

    args = parse_arguments()
    # If verbose logging was requested, enable it as soon as possible
    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
        logging.debug('Verbose logging enabled.')
        logging.debug(f'Command line arguments parsed. Their values are:\n{pprint.pformat(args)}')
    if not check_argument_validity(args):
        sys.exit('Exiting because of invalid command line arguments.')

    # Determine list of input files to process
    if len(args.files) > 0:
        logging.debug(f'Choosing files based on file list of {len(args.files)} files.')
        file_list = args.files
    else:
        logging.debug(f'Choosing files based on extension "{args.extension}".')
        file_list = get_files_by_extension(args.extension)
    
    logging.debug(f'List of files to process:\n{file_list}')

    # Process the files
    if len(file_list) == 0:
        logging.info('No input files to process.')
        return
    
    for in_file in file_list:
        out_file = generate_output_file_name(
            file_name=in_file,
            postfix=args.postfix,
            output_dir=args.output_dir
        )

        if out_file == in_file:
            logging.error(f'Input and output file names "{out_file}" are the same. Skipping.')
            continue
        
        process_file(in_file, out_file, args.overwrite)


def show_greeting():
    """Prints a greeting with script information to the standard output."""

    print(f'{Script.NAME} version {Script.VERSION} ({Script.DATE})\n')



def check_argument_validity(args: Namespace) -> bool:
    """Checks whether the parsed command line arguments `args` are valid. Returns `True`
    if they are valid and `False` otherwise."""

    logging.debug('Checking validity of command line arguments.')
    args_valid = True

    # Check that the user provided either file list to process or a file extension
    files_provided = len(args.files) > 0
    extension_provided = args.extension != None

    if files_provided and extension_provided:
        logging.critical('Both input file list and file extension of files to process were '
            'provided. Only one of those should be specified.')
        args_valid = False
    elif not files_provided and not extension_provided:
        logging.critical('No input file list or file extension of files to process were '
            'provided.')
        args_valid = False
    
    # Check that exactly one of file extension or output dir was provided
    output_dir_provided = args.output_dir != None
    postfix_provided = args.postfix != None

    if not (output_dir_provided or postfix_provided):
        logging.critical('At least one of output dir or postfix has to be provided to '
            'generate the names of output files.')
        args_valid = False

    # If output dir was provided, check that it exists
    if output_dir_provided:
        output_dir = args.output_dir
        if not os.path.isdir(output_dir):
            logging.critical(f'Provided output directory "{output_dir}" does not exist.')
            args_valid = False

    # Final evaluation and value return
    if args_valid:
        logging.debug('The arguments are valid.')
        return True
    else:
        logging.debug('The arguments are invalid.')
        return False


def get_files_by_extension(extension: str) -> list[str]:
    """Lists the current working directory and returns a list of file names that
    match `extension`."""

    abspath = os.path.abspath('.')
    logging.debug(f'Listing current working directory("{abspath}") for files.')
    dir_listing = os.listdir('.')

    file_list = []

    for file in dir_listing:
        if file.endswith(extension):
            file_list.append(file)
    
    logging.debug(f'Found {len(file_list)} files mathing extension "{extension}".')
    return file_list


def process_file(in_file: str, out_file: str, overwrite=False) -> bool:
    """Reads input file `in_file`, processes it, and writes the result to output file
    `out_file`. Returns `True` if no errors occured and `False` otherwise.

    If the output file already exists, it will only be overwritten if `overwrite` is `True`.
    """

    logging.info(f'Processing input file "{in_file}", writing to output file "{out_file}".')

    # Check if output file already exists
    if os.path.exists(out_file):
        if not overwrite:
            logging.error(f'File "{out_file}" already exists and will not be overwritten.')
            return False
        else:
            logging.info(f'Output file "{out_file}" will be overwritten.')

    try:
        # Manually open and close files to have the flexibility of reopening the input
        # file with non-default encoding
        fi = open(in_file)
        logging.debug(f'Opened input file "{in_file} with default encoding".')
        fo = open(out_file, mode='w')
        logging.debug(f'Opened output file "{out_file}."')

        # Loop needed to retry opening the input file with different encoding
        while True:
            line_count = 0
            # Whether the tag "#DATA" has been found
            found_data = False

            try:
                # Process the input file line by line
                for line in fi:
                    line_count += 1
                    line = line.strip()

                    # Process line based on whether data tag has been reached
                    if found_data:
                        result = process_data_line(line)
                        if isinstance(result, str):
                            fo.write(result + '\n')
                    else:
                        if line == '#DATA':
                            found_data = True
                            logging.debug(f'Found data tag in line {line_count}.')
                
                # Warn the user if data tag has not been reached by the end of the file
                logging.warning(f'Data tag has not been reached in input file "{in_file}".')

                # File processed successfully, no need to repeat
                break

            except UnicodeDecodeError as e:
                logging.warning(f'Could not decode line {line_count} of file '
                    f'"{in_file}": {str(e)}.')
                
                fi.close()
                logging.debug(f'File "{in_file}" closed.')
                logging.info('Retrying to open the input file with encoding "iso-8859-1".')
                fi = open(in_file, encoding='iso-8859-1')

                logging.debug(f'Closing and reopening output file "{out_file}" to truncate it.')
                fo.close()
                fo = open(out_file, mode='w')

    except FileNotFoundError as e:
        logging.error(f'Input file "{e.filename}" does not exist.')
        return False
    except OSError as e:
        logging.error(f'Could not open file "{e.filename}": {e.strerror}')
        return False
    finally:
        try:
            fi.close()
            logging.debug(f'Input file "{in_file}" closed.')
        except NameError:
            logging.error(f'Could not close input file "{in_file}" (variable does not exist).')
        
        try:
            fo.close()
            logging.debug(f'Output file "{out_file}" closed.')
        except NameError:
            logging.error(f'Could not close output file "{out_file}" (variable does not exist).')

    return True


def generate_output_file_name(file_name: str, postfix=None, output_dir=None) -> str:
    """Generates path to the output file from input file name `file_name` and returns it.

    If `postfix` (type `str`) is specified, it is appended to the output file name before
    the extension. If `output_dir` (also `str`) is specified, the resulting file name is
    joined to its path.

    Only the file name of the input file is used. If the file is located in a subdirectory,
    the subdirectory name is stripped from the generated output path.
    """

    result = file_name

    if postfix != None:
        base_name = os.path.basename(file_name)
        base, extension = os.path.splitext(base_name)
        result = base + postfix + extension

    if output_dir != None:
        result = os.path.join(output_dir, result)

    return result


def process_data_line(line: str):
    """Processes data line by replacing potential decimal commas with decimal dots.
    Returns the processed line as `str` or None in case of errors."""

    split = line.strip().split()

    if len(split) != 2:
        logging.error(f'Data line "{line}" was split into {len(split)} words, while '
            'there should only be 2 words.')
        return None

    # Replace decimal commas with decimal dots and try to convert the values to float.
    # Replace the values in list `split` with parsed floats.
    for i in range(len(split)):
        value_str = split[i].replace(',', '.')
        try:
            value_float = float(value_str)
            split[i] = value_float
        except ValueError:
            logging.error(f'Could not parse value "{split[i]}".')
            return None

    wavenumber = split[0]
    transmitance = split[1]
    format_string = '{:12.6f} {:10.6f}'
    result = format_string.format(wavenumber, transmitance)

    return result


if __name__ == "__main__":
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    main()
